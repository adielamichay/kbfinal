<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use app\models\Product;
use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;

$this->title = 'SingleProduct';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    
    DetailView::widget([
        'model' => $model
        
    ]);
    
    ?> 

    <code><?= __FILE__ ?></code>
</div>
